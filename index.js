// Fetch request GET Method
fetch("https://jsonplaceholder.typicode.com/todos/", {
	method: "GET"
})
.then((response) => response.json())
.then((json) => {
	let titles = json.map(item => item.title);
	console.log(titles);
});


// Fetch to do list GET method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "GET"
})
.then((response) => response.json())
.then((json) => console.log(json))
.then((json) => console.log("The item 'delectus aut autem' on the list has a status of false"));


// Fetch request POST
fetch("https://jsonplaceholder.typicode.com/todos/", {
	method: "POST",
	headers: {
		"Content-Type": "application/json",
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then((json) => console.log(json));

// Fetch request PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		status: "Pending",
		description: "To update my to do list with a different data structure",
		dateCompleted: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21",
		title: "delectus aut autem",
		userId: 1
	})
})
.then(response => response.json())
.then((json) => console.log(json));

// Delete method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});

